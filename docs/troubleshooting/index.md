---
hide:
  - navigation
description: >-
  Common issues summarized with solutions when available, to help WSL docker users.
---

# Troubleshooting

This section gathers issues and solutions reported by users and for now roughly imported here.

## Networking

Once in a while, DNS resolution within docker containers breaks down.

> WSL seems to have a networking bug where at some point it gets confused in how to serve
> DNS to instances. This can usually be resolved with a wsl --shutdown (or windows restart).
> You can likely have a more permanent fix by setting static DNS in `/etc/resolv.conf`
> Another option but less ideal may be disabling IPv6 on all your network adapter in Windows.

## Various

Docker command failures when Rancher Desktop is or was installed:

> Rancher Desktop even if not enabled on a specific WSL distribution, can override a few
> docker executables through making them available in your wsl $PATH, before the WSL own
> binaries.

WSL commands error out with policies prevented user from starting shell process:

> If you don't mind loosing WSL data, you can uninstall and re-install WSL and its
> components to solve that misleading error. You can also avoid loosing WSL distribution
> data by uninstalling a recent WSL update and reinstalling it.
