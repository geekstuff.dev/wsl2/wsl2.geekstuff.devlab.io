---
title: Linux IT Dev
hide:
  - navigation
  - toc
description: >-
  Info on IT Development using WSL and pre-built batteries docker-dev images that takes
  official Ubuntu (20.04, 22.04, 22.04-systemd, 24.04-systemd) and Alpine (3.17, 3.20) distributions and bakes
  in a docker engine, ssh-agent, gpg-agent and a docker-dev cli to help out with more tasks.
---

# Linux IT Dev on WSL

This site contains information on working with Linux / Docker IT solutions in Windows WSL2,
in the most efficient, stable, simplest and standard Linux way, with everything embedded
inside your WSL distribution, compatible with VSCode IDE which can natively run within
your WSL Linux distribution.

If your read all of MS docs on WSL, you will find that the same approaches are promoted and
additionally simplified here.

## Setup a complete docker-dev environment in 5 minutes

With WSL subsystem already installed in your updated Windows 10 or 11, you can setup pre-built
WSL images with all the basic IT development bits you need to do anything that works in Linux,
docker or kubernetes.

Simply follow the instructions generated in the latest release of one (or all) of the following
distributions:

{% for image in docker_dev_images_links %}
- [{{ image.name }}](./docker-dev/import/{{ image.path }})
{% endfor %}

For more details on what is included and how to enable WSL preview version with systemd
support, see [this README](https://gitlab.com/geekstuff.dev/wsl2/common/dev-setup/-/blob/v0.1.5/README.md).

## Features included in pre-built images

- A docker engine, started either on user login or through systemd depending
- Docker compose, ssh-agent, gpg-agent, latest git
- Starting configs for ssh, git and gpg
- Compatible with VScode running in WSL
- Compatible with Devcontainers
- Compatible with any Linux instructions on anything
- A `docker-dev info` and other commands to make quick work of setting up gpg git commit
  signing, create a local KinD k8 cluster and more.

## FAQ

How do I keep track of any new updates on these images?

> The best way for the moment is to use a Gitlab account and turn on notifications on the
> relevant GitLab project, and also to add stars to gitlab projects not only for encouragement
> but also to get some idea on usage.

Can I use the docker engine baked in these images, from Windows?

> Yes. You can make 1 minor modification to a single WSL image /etc/docker/daemon.json config
> so that it listens to SOCK, TCP, or even both. But.. there are many more steps to tackle
> before you can use "docker" commands in your windows host, which leads to a bunch of
> opportunities for the setup to break, a huge IO penalty as well if using /mnt/c/ which
> you will certainly feel on any sizeable project, no linux file watch since not a linux fs,
> and more roadblocks or issues down the line to tackle. Definitely feasible but requires
> a lot of things to do on Windows side, which may not be necessary in many cases.
>
> What is promoted here instead are fully self-contained WSL images, having code next to
> your docker engine, no /mnt/c drive for top IO performance and 100% linux capabilities,
> with VSCode awesome ability to launch itself within a WSL distribution, and also launch
> devcontainers.

Can I copy this and do my own thing?

> Absolutely! But there are more features and improvements that will come here such as
> ability for an imported distribution to upgrade itself to a new "docker-dev" version. The
> recommendation is definitely to combine efforts and/or keep track of new versions.
>
> An example extending this and building on top of is also under way and will help iron
> out any adjustments that could be made here to make that "extending" as smooth as possible.

What about Docker/Rancher Desktop?

> What is promoted here is self-contained WSL images with its own docker engine embedded inside
> it, exactly like you would have in a real Linux installation. You can run multiple WSL images
> with each their own docker engine with absolutely no issues if done right. You can even use
> Docker Desktop at the same time although that would not make much sense. Docker Desktop can
> provide a docker engine to multiple WSL distributions. It does this well and is part of what
> you pay for.
>
> For Rancher Desktop it can in theory co-exist with Rancher desktop but in practice there
> can be issues even if RD is not enabled on an imported WSL distribution, since it provides
> its cli binaries through PATH, overriding WSL own paths. (not cool)
