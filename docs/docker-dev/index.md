---
hide:
  - toc
description: >-
  List of instructions to setup a pre-built WSL Docker-Dev images in 5 minutes, with a
  docker engine, ssh-agent, gpg-agent, fully powered by Linux and ready to work on any
  Linux, Docker or Kubernetes projects.
---

# WSL Docker-Dev builds

Select one of the WSL [Docker-Dev](https://gitlab.com/geekstuff.dev/wsl2/common/dev-setup/-/blob/v0.1.5/README.md)
build to see its installation instructions and in under 5 minutes get a docker engine, ssh-agent
gpg-agent, fully powered by Linux and ready to work on any Linux, Docker or Kubernetes projects.

{% for image in docker_dev_images_links %}
- [{{ image.name }}](./import/{{ image.path }})
{% endfor %}
