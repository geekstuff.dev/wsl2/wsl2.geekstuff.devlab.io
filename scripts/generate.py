#!/usr/bin/env python3

import sys
sys.path.append('src/')

from data import get_images
from generator import DocGenerator

def generate():
    DocGenerator(get_images()).generate()

def on_startup(command, dirty):
    generate()

# Run if script is called directly
if __name__ == '__main__':
    generate()
