import re, gitlab.v4.objects.projects as gitlab_project

class DockerDevImage:
    templateDescriptionRegex = r"^template_description_.*\.md$"

    def __init__(self, gitlabProject: gitlab_project.Project):
        self.image = gitlabProject
        self.latestRelease = self.fetch_latest_release()

    def name(self):
        return self.image.name

    def path(self):
        return self.image.path

    def description(self):
        return self.image.__getattr__("description")

    def release_tag_link(self):
        return "[{} release]({})".format(
            self.latestRelease.get("tag"),
            self.latestRelease.get("tag_url"),
        )

    def howto_url(self):
        return self.latestRelease.get("description_url")

    def create_date(self) -> str:
        return self.latestRelease.get("create_date")

    def update_date(self) -> str:
        return self.latestRelease.get("update_date")

    def fetch_latest_release(self):
        latestRelease = {
            "tag": "",
            "tag_url": "",
            "description_url": "",
            "create_date": self.image.created_at,
            "update_date": self.image.created_at,
        }
        release = ""

        for release in self.image.releases.list(Iterator=True):
            latestRelease["tag"] = release.tag_name
            latestRelease["tag_url"] = release.__getattr__("_links")["self"]
            latestRelease["update_date"] = release.released_at
            break

        if release:
            for link in release.attributes.get("assets")["links"]:
                if re.match(self.templateDescriptionRegex, link["name"]):
                    latestRelease["description_url"] = link["url"]
                    break

        return latestRelease
