import image, images, jinja2, os
from jinja2 import Environment, FileSystemLoader

class DocGenerator:
    generated_dir = "docs/docker-dev/import"

    def __init__(self, images: images.DockerDevImages):
        self.images = images
        self.templates = self.get_templates()

    def generate(self):
        print("generate class")
        self.cleanup()
        os.makedirs(self.generated_dir, exist_ok=True)
        self.generate_pages()
        self.generate_images()

    def cleanup(self):
        for root, dirs, files in os.walk(self.generated_dir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))

    def get_templates(self) -> dict[str, jinja2.Template]:
        environment = Environment(
            loader=FileSystemLoader("templates/"),
        )

        return {
            "image": environment.get_template("tpl.image.md"),
            "pages": environment.get_template("tpl.image.pages"),
        }

    def generate_pages(self):
        filename = f"{self.generated_dir}/.pages"
        content = self.templates["pages"].render({
            "images": self.images.iterate(),
        })
        with open(filename, mode="w", encoding="utf-8") as message:
            message.write(content)

        print(f"generated [{filename}]")

    def generate_images(self):
        for image in self.images.iterate():
            self.generate_image(image)

    def generate_image(self, image: image.DockerDevImage):
        filename = f"{self.generated_dir}/{image.path()}.md"
        content = self.templates["image"].render({
            "title": image.name(),
            "description": image.description(),
            "release_tag_link": image.release_tag_link(),
            "howto_url": image.howto_url(),
            "create_date": image.create_date(),
            "update_date": image.update_date(),
        })
        with open(filename, mode="w", encoding="utf-8") as message:
            message.write(content)

        print(f"generated [{filename}]")
