import gitlab, image
from collections import OrderedDict
from collections.abc import Generator

class DockerDevImages:
    def __init__(self, gitlabGroupID: int):
        client = gitlab.Gitlab()
        group = client.groups.get(gitlabGroupID)
        images = {}
        for each in group.projects.list(iterator=True,per_page=50,visibility="public"):
            project = client.projects.get(each.get_id())
            if "org.publish" not in project.__getattr__("topics"):
                continue
            images[project.name] = image.DockerDevImage(project)
        self.images = OrderedDict(sorted(images.items(), key=lambda t: t[0]))

    def iterate(self) -> Generator[image.DockerDevImage]:
        for image_key in self.images:
            yield self.images[image_key]

    def list_title_markdown(self) -> dict[str, str]:
        data = []
        for image in self.iterate():
            data.append({
                "name": image.name(),
                "path": f"{image.path()}.md",
            })
        return data
