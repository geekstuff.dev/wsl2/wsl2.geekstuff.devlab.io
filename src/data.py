import gitlab, image, images

group_id = 60181446
initialized = False

def get_images() -> images.DockerDevImages:
    global initialized, obj
    if initialized != True:
        obj = images.DockerDevImages(group_id)
        initialized = True

    return obj
