import sys
sys.path.append('src/')
from data import get_images

def define_env(env):
    env.variables['docker_dev_images_links'] = get_images().list_title_markdown()
